<?php echo $this->element('ban_admin'); ?>

    <h1>Categories</h1>
      <ul>
      <?php
      $menu = $this->requestAction(array("controller" => "bcategories", "action" => "clist"));
      foreach ($menu as $m)
      {
        $m=$m['Bcategory'];
        echo "<li>".$this->Html->link($m['titre'], array('controller' => 'posts', 'action' => 'category',$m['id'])).'</li>';
      }
      
      ?>
      </ul>
<?php
echo $this->Html->link(array('action' => 'index'));
?>
<h1>Edition d'articles</h1>

<ul>
	<?php
	foreach($articles as $a)
	{
		$a = $a['Post'];
		echo "<li>".$this->Html->link(utf8_encode($a['titre']), array('action' => 'edit', $a['id']))."--".$this->Html->link('[x]', array('action' => 'delete', $a['id']), null, "Voulez vous vraiment supprimer cet article")."</li>";
	}