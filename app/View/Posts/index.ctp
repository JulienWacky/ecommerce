<?php echo $this->element('ban_user'); ?>
    <!-- <div id="layout" class="pure-g"> -->
    <div class="sidebar pure-u-med-1-6">
        <div class="header">
            <hgroup>
                <h1 class="brand-title">eRabbit Conseil</h1>
                <h2 class="brand-tagline">Des conseils pour vous et vos lapins</h2>
            </hgroup>
            <p>Categories :</p>
            <nav class="nav">
                <ul class="nav-list">
                    <?php
                    $menu = $this->requestAction(array("controller" => "bcategories", "action" => "clist"));
                    foreach ($menu as $m)
                  {
                    $m=$m['Bcategory'];
                    echo "<li class='nav-item cat'>".$this->Html->link($m['titre'], array('controller' => 'posts', 'action' => 'category',$m['id'])).'</li>';
                  } ?>
                </ul>
            </nav>
        </div>
    </div>
    <!-- <div class="content pure-u-1-2 pure-u-med-1-4"> -->
      <div>
      <div class="content pure-u-1-2 pure-u-med-3-4">
    <h1 class="content-subhead">Articles</h1>
    <?php foreach($articles as $a): ?>
    
        <div>
            <!-- A wrapper for all the blog posts -->
            <div class="posts">
    <section class="post">
                    <header class="post-header">
                        <h2 class="post-title"><?php echo $this->Html->link($a["Post"]["titre"], array('action' => 'voir', $a['Post']['id'])); ?></h2>
                        <p class="post-meta">
                            By <a class="post-author" href="#">Andrew Wooldridge</a> under 
                            <!-- <a class="post-category post-category-yui" href="#"> -->
                               <span class="post-category post-category-yui catcat" style="color:white;"><?php echo $this->Html->link($a['Bcategory']['titre'], array('class'=>'post-category post-category-yui', 'controller' => 'posts', 'action' => 'category',$a['Post']['bcategory_id']));?></span>
                              
                            <!-- </a> -->

                          <!--   <?php echo $this->Html->link($a['titre'], array('class'=>'pure-button', 'controller' => 'posts', 'action' => 'category',$m['id'])); ?> -->
                        </p>
                    </header>

                    <div class="post-description">
                        <p>
                            <?php echo $a["Post"]["contenu"];?>
                        </p>
                    </div>
                </section>
            </div>

    <?php endforeach;?>
    </div>
    </div>