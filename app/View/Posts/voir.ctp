<?php echo $this->element('ban_user'); ?>
    <div class="sidebar pure-u-med-1-6">
        <div class="header">
            <hgroup>
                <h1 class="brand-title">eRabbit Conseil</h1>
                <h2 class="brand-tagline">Des conseils pour vous et vos lapins</h2>
            </hgroup>
            <p>Categories :</p>
            <nav class="nav">
                <ul class="nav-list">
                    <?php
                    $menu = $this->requestAction(array("controller" => "bcategories", "action" => "clist"));
                    foreach ($menu as $m)
                  {
                    $m=$m['Bcategory'];
                    echo "<li class='nav-item cat'>".$this->Html->link($m['titre'], array('class'=>'pure-button', 'controller' => 'posts', 'action' => 'category',$m['id'])).'</li>';
                  } ?>
                </ul>
            </nav>
        </div>
    </div>
    <!-- <h1>Categories</h1>
      <ul>
      <?php
      $menu = $this->requestAction(array("controller" => "bcategories", "action" => "clist"));
      foreach ($menu as $m)
      {
        $m=$m['Bcategory'];
        echo "<li>".$this->Html->link($m['titre'], array('controller' => 'posts', 'action' => 'category',$m['id'])).'</li>';
      }
      
      ?>
      </ul> -->
      <div>
      <div class="content pure-u-1-2 pure-u-med-3-4">
    <h1 class="content-subhead">Commentaires</h1>
     <div>
<!--       <h1><?php var_dump($a['Post'])?></h1> -->
            <!-- A wrapper for all the blog posts -->
            <div class="posts">
              <section class="post">
                    <header class="post-header">
                        <h2 class="post-title"><?php echo $a['Post']['titre']; ?></h2>
                        <p class="post-meta">
                            By <a class="post-author" href="#">Andrew Wooldridge</a> under <a class="post-category post-category-yui" href="#"><?php echo $a['Bcategory']['titre'];?></a>
                        </p>
                    </header>

                    <div class="post-description">
                        <p>
                            <?php echo $a["Post"]["contenu"];?>
                        </p>
                    </div>
                </section>
            </div>
            <?php var_dump($a);?>
            <?php foreach($a['Comment'] as $c)
                  {
                      echo '<strong>De : '.$c['pseudo'].'</strong>';
                      echo '<p>-> '.$c['contenu'].'</p>';
                  }
            ?>


<?php
echo $this->Html->link('Retour aux conseils', array('action' => 'index'));
?><!-- 
<?php

echo '<div class="msg"><p><span class="titre">'.$this->Html->link($a["Post"]["titre"], array('action' => 'voir', $a['Post']['id'])).'</span>';
echo '<br/><span class="info"> Le '.date('d / m / Y',strtotime($a["Post"]["date"])).'::'.$a['Bcategory']['titre'].'</span>';
echo '</p></div>';
echo '<p>'.$a["Post"]["contenu"].'</p>';

    foreach($a['Comment'] as $c)
    {
        echo '<strong>'.$c['pseudo'].'</strong>';
        echo '<p>'.$c['contenu'].'</p>';
    }
?>
 -->
<div id="goCommentaireBlog">
  <h2>Laisser un commentaire -><h2>
</div>
<div id="commentaireBlog">
<?php

echo $this->Form->create('Comment', array('class' => "pure-form pure-form-stacked", 'url'=>array('controller' => 'posts', 'action' => 'voir', $a['Post']['id'])));
echo $this->Form->input('pseudo');
echo $this->Form->input('mail');
echo $this->Form->input('contenu');
echo $this->Form->input('post_id', array('type'=>'hidden','value' => $a['Post']['id']));

echo $this->Form->submit('Envoyer', array('class'=>"pure-button buttonRose"));


?>

</div>
  <?php echo $this->Html->script('script');
      echo $this->Html->script('jQuery');   ?>