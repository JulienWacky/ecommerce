<?php echo $this->element('ban_admin'); ?>
<?php echo $this->element('menu_admin'); ?>

    	<h2> GESTION CATEGORIE</h2>
    <h3>► Categories</h3>
      <ul class="nav-list">
      <?php
      $menu = $this->requestAction(array("controller" => "bcategories", "action" => "clist"));
      foreach ($menu as $m)
      {
        $m=$m['Bcategory'];
        echo "<li> → ".$this->Html->link($m['titre'], array('controller' => 'posts', 'action' => 'category',$m['id'])).'</li>';
      }
      
      ?>
      </ul>
<br>
<h3>► Edition d'articles</h3>

  <table class="pure-table">
         <thead>
          <tr>
            <th>Articles</th>
            <th>Action</th>
          </tr>
        </thead>
       
    <?php
    foreach($articles as $a)
    {
        $a = $a['Post'];
        echo "<tr><td>".$this->Html->link(utf8_encode($a['titre']), array('action' => 'edit', $a['id']))."</td>"."<td>".$this->Html->link('Supprimer', array('action' => 'delete', $a['id']), null, "Voulez vous vraiment supprimer cet article")."</td></tr>";
    }

    ?>
    
    </table>
</div>