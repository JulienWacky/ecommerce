<?php echo $this->element('ban_user'); ?>
 	<div id="recappanier">
    <h1>Mon panier</h1>
    <table class="pure-table">
       <thead>
        <tr>
            <th class="thProduit">Produit</th>
            <th class="thPrix">Prix</th>
            <th class="thQuantite">Quantité</th>
        </tr>
       </thead>
    <?php $totalPrice = 0; ?>
    <?php 
                  $cart = $this->Session->read('Cart');
                 if($cart == NULL) { 
                 	$cart = array();
                 }
                 foreach ($cart as $key => $product): ?>
        <tr>
            <td>
                <?php
                  echo $this->Html->link($product['Product']['name'], array('action'=> 'view', $product['Product']['id'])); 
                ?>
            </td>
            <td>
                <?php 
                  echo $product['Product']['price']; 
                ?>
            </td>
          <td>
            <?php
              echo $product['Product']['quantity'];
            ?>
          </td>
        </tr>
        <?php 
   
        $totalPrice = $totalPrice + $product['Product']['price']."€"; 
        ?>
    <?php endforeach; ?>
        <tr>
            <th>Prix Total: </th>
            <th><?php 
               echo $totalPrice; 
               ?>
            </th>
            <th></th>
        </tr>
    </table>
</div>
 <div id="commande">
    <h2 style="color:#FF00FF;" id="goCommande">En cours d'adoption ></h2>
   
    <div id="commandeform">
      <div class="form1">
    <br><br>
      <h2 style="color:#FF00FF;">Informations client</h2><br>
      <?php echo $this->form->create('Order', array('url' => '/orders/index','class' => "pure-form pure-form-stacked")); ?>
<?php echo $this->Form->input('first_name', array('class' => 'form-control', 'label'=> "Prenom : ")); ?>
<br />
<?php echo $this->Form->input('last_name', array('class' => 'form-control', 'label'=> "Nom : ")); ?>
<br />
<?php echo $this->Form->input('email', array('class' => 'form-control', 'label'=> "Email : ")); ?>
<br />
<?php echo $this->Form->input('phone', array('class' => 'form-control', 'label'=> "Numero Tel. : ")); ?>
<br />
<br />
</div>

<div class="form1">
  <br><br>
<h2 style="color:#FF00FF;">Vos coordonnées</h2><br>
<h3>► Facturation</h3><br>
<?php echo $this->Form->input('billing_address', array('class' => 'form-control pure-form pure-form-stacked', 'label'=> "Adresse facturation : ")); ?>
<br />
<?php echo $this->Form->input('billing_city', array('class' => 'form-control', 'label'=> "Ville : ")); ?>
<br />
<?php echo $this->Form->input('billing_zip', array('class' => 'form-control', 'label'=> "Code Postal : ")); ?>
<br />
<?php echo $this->Form->input('billing_country', array('class' => 'form-control', 'label'=> "Pays : ")); ?>
<br />
<br />
</div>

<div class="form1">
  <br><br><br><br><br><br>
<h3>► Livraison</h3><br>
<?php echo $this->Form->input('shipping_address', array('class' => 'form-control', 'label'=> "Adresse livraison : ")); ?>
<br />
<?php echo $this->Form->input('shipping_city', array('class' => 'form-control', 'label'=> "Ville: ")); ?>
<br />
<?php echo $this->Form->input('shipping_zip', array('class' => 'form-control', 'label'=> "Code postal : ")); ?>
<br />
<?php echo $this->Form->input('shipping_country', array('class' => 'form-control', 'label'=> "Pays : ")); ?>
<br />
</div>

<div class="form1">
    <br><br><br><br><br><br>
  <h3>► Payement</h3>
<?php echo $this->Form->input('numero_carte', array('class' => 'form-control', 'label'=> "Numéro carte : ")); ?><br>
<?php echo $this->Form->input('date_expiration', array('class' => 'form-control', 'label'=> "Date d'expiration : ")); ?><br>
<?php echo $this->Form->input('code_secu', array('class' => 'form-control', 'label'=> "Code de sécurité : ")); ?><br>
<?php echo $this->form->submit("COMMANDER", array('class'=>"pure-button buttonRose")); ?>
</div>
 <?php echo $this->Html->link('Facture', array('action' => 'viewPdf')); ?>
</div>

 </div>
