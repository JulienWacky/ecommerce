<?php
/* display message saved in session if any */
echo $this->Session->flash();
?>

<h2>Cart</h2>
<table>
    <tr>
        <th>Name</th>
        <th>Price</th>
        <th>Action</th>
    </tr>
    <?php $totalPrice = 0; ?>
    <?php foreach ($cart as $key => $product): ?>
    <tr>
        <td>
            <?php 
              //link to product page
              echo $this->Html->link($product['Product']['name'], array('action'=> 'view', $product['Product']['id'])); 
            ?>
        </td>
        <td>
            <?php 
              //show product price
              echo $product['Product']['price']; 
            ?>
        </td>
        <td>
            <?php 
              //remove product from a cart
              echo $this->Html->link('delete', array('action' => 'delete',$key)); 
            ?>
        </td>
    </tr>
    <?php 
    //calculate total price of all products in a cart
    $totalPrice = $totalPrice + $product['Product']['price']; 
    ?>
<?php endforeach; ?>
    <tr>
        <th>Total Price: </th>
        <th><?php 
          //show total price
           echo $totalPrice; 
           ?>
        </th>
        <th>
          <?php
            //delete all elements from a cart
            echo $this->Html->link('empty', array('action'=>'empty_cart'));
          ?>
        </th>
    </tr>
</table>

<div>
    <?php 
      //link to products page
      // echo $this->Html->link('Pages', array('action' => 'home')); 
    ?>
</div>