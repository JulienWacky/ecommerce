    <div class="banner2">
        <h1 class="banner-head">
            Keep calm.<br>
            You are admin.
        </h1>
    </div>

    <div class="panel">

<div class="pure-menu pure-menu-open" style="width:250px;height:250px;;margin-left:100px;margin-right:50px;float:left;margin-top:5px;">
    <a class="pure-menu-heading"><b>PANEL ADMIN</b></a>

    <ul>
        
        <li class="pure-menu-heading"><?php echo $this->Html->link(__('GESTION CATEGORIE'), array('controller' => 'categories', 'action' => 'panel_categorie')); ?></li>
		<li class="pure-menu-heading"><?php echo $this->Html->link(__('GESTION de Produits'), array('controller' => 'categories', 'action' => 'panel_produit')); ?></li>
        <li class="pure-menu-heading"><?php echo $this->Html->link(__('Gestion Utilisateurs'), array('controller' => 'categories', 'action' => 'panel_user')); ?></li>
    </ul>

</div>

<h2> GESTION CATEGORIE</h2>
<?php echo $this->Html->link(__('AJOUTER SOUS CATEGORIE'), array('controller' => 'categories', 'action' => 'add')); ?>

<table class="pure-table pure-table-bordered" style="float:left;width:1000px;margin-right:100px;margin-top:5px;">
<tr>
			<th><?php echo $this->Paginator->sort('parent_id'); ?></th>

			<th><?php echo $this->Paginator->sort('name'); ?></th>
			<th><?php echo $this->Paginator->sort('description'); ?></th>
			<th class="actions"><?php echo __('Actions'); ?></th>
	</tr>
	<?php foreach ($categories as $category): ?>
	<tr>
		<td>
			<?php echo $this->Html->link($category['ParentCategory']['name'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?>
		</td>
		<td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['description']); ?>&nbsp;</td>
<!-- 		<td><?php echo h($category['Category']['created']); ?>&nbsp;</td>
		<td><?php echo h($category['Category']['modified']); ?>&nbsp;</td> -->
		<td class="actions">
			<?php echo $this->Html->link(__('+'), array('action' => 'view', $category['Category']['id'])); ?>
			<?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $category['Category']['id'])); ?>
			<?php echo $this->Form->postLink(__('Suprimer'), array('action' => 'delete', $category['Category']['id']), null, __('Etes vous sur de suprimer?', $category['Category']['id'])); ?>
		</td>
	</tr>
<?php endforeach; ?>
</table>

</div>