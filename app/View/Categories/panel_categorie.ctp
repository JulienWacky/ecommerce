<?php echo $this->element('ban_admin'); ?>
<?php echo $this->element('menu_admin'); ?>

    	<h2> GESTION CATEGORIE</h2>

    	<table class="pure-table pure-table-bordered" style="float:left;width:1000px;margin-right:100px;margin-top:5px;">
    		<tr>
                <thead>
    			<th><?php echo $this->Paginator->sort('parent_id'); ?></th>

    			<th><?php echo $this->Paginator->sort('name'); ?></th>
    			<th><?php echo $this->Paginator->sort('description'); ?></th>
    			<th class="actions"><?php echo __('Actions'); ?></th>
            </thead>
    		</tr>
    		<?php foreach ($categories as $category): ?>
    		<tr>
    			<td>
    				<?php echo $this->Html->link($category['ParentCategory']['name'], array('controller' => 'categories', 'action' => 'view', $category['ParentCategory']['id'])); ?>
    			</td>
    			<td><?php echo h($category['Category']['name']); ?>&nbsp;</td>
    			<td><?php echo h($category['Category']['description']); ?>&nbsp;</td>
<!-- 		<td><?php echo h($category['Category']['created']); ?>&nbsp;</td>
	<td><?php echo h($category['Category']['modified']); ?>&nbsp;</td> -->
	<td class="actions">
		<?php echo $this->Html->link(__('+'), array('action' => 'view', $category['Category']['id'])); ?>
		<?php echo $this->Html->link(__('Editer'), array('action' => 'edit', $category['Category']['id'])); ?>
		<?php echo $this->Form->postLink(__('Suprimer'), array('action' => 'delete', $category['Category']['id']), null, __('Etes vous sur de suprimer?', $category['Category']['id'])); ?>
	</td>
</tr>
<?php endforeach; ?>
</table>

</div>