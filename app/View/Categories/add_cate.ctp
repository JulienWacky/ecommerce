<?php echo $this->element('ban_admin'); ?>
        <?php echo $this->element('menu_admin'); ?>

    <div class="categories blockadmin">
    	<?php echo $this->Form->create('Category', array('class' => "pure-form pure-form-stacked")); ?>
    		<h2>Ajout Categorie</h2>
    		<?php
    		echo $this->Form->input('name');
    		echo $this->Form->input('description');
    		?>
    		<br>
    		<?php echo $this->Form->submit(__('Submit', array('class'=>"pure-button"))); ?>
    </div>
</div>