<?php echo $this->element('ban_user'); ?>
<div class="categories view">
</div>
<br>
<div class="related">
	<h1 class="categorieName">► <?php echo __($category['Category']['name']); ?></h1>
	<br>
	<?php if (!empty($category['Product'])): ?>
		<?php foreach ($category['Product'] as $product): ?>
		<div class="product pure-u-1-5">
			<div class="image pure-u-4-24">
				<?php echo $this->html->image($product['image'], array( 'class' => 'img_min'));  ?>
			</div>
			<div class="name">
				<h1><?php echo $this->Html->link(__($product['name']), array('controller' => 'products', 'action' => 'view', $product['id'])); ?></h1>
			</div>
			<div class="price">
				<b><?php echo $product['price']."€";?></b>
				<a href=" <?php $this->Html->link( array('controller' => 'products', 'action' => 'view', $product['id'])); ?>" ><br>
					<button style="background:#ffe3f5;" class="pure-button buttonRose"><b>SEE MORE</b></button></a>
			</div>
		</div>
	<?php endforeach; ?>
<?php endif; ?>
<br>
</div>
