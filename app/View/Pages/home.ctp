<br><br><br>
    <div class="slider">
        <ul class="slides">
            <li class="slide"><div class="box" style="background-color: #fff;"><img src="/img/06.jpg"/></div></li>
            <li class="slide"><div class="box" style="background-color: #fff;"><img src="/img/01.jpg"/></div></li>
            <li class="slide"><div class="box" style="background-color: #fff;"><img src="/img/04.jpg"/></div></li>
        </ul>
    </div>

    <script>
    $('.slider').glide({
        autoplay: 5000,
        arrows: 'body',
        nav: 'body'
    });
    var glide = $('.slider').glide({
        afterTransition: function() {
            var currentSlide = this.currentSlide;
            console.log(currentSlide);
        }
    }).data('api_glide');

    $(window).on('keyup', function (key) {
        if (key.keyCode === 13) {
            glide.jump(3, console.log('Wooo!'));
        };
    });

    $('.slider-arrow').on('click', function() {
        console.log(glide.current());
    });
</script> <br><br><br>
<div class="l-content">
  <div class="block">
    <h2>► Erabbit</h2>
    <!-- <span class="pricing-table-price "> --><i><p style ="font-size:20px;">Le lapin est un animal qui aime la compagnie de l'humain.  Il passera beaucoup de temps à vous surveiller et apprendra rapidement vos habitudes et s'y adaptera très bien.  Il se reposera le jour et gardera sont énergie pour vous tenir compagnie le soir.  Le lapin aime bien regarder la télévision sur les genoux de son maître, gambader dans la maison en fouinant un peu partout et s'endormira près de vous si vous lui en donnez l'occasion.

    Il ne faut pas oublier que le lapin est une proie, il faut donc lui prouver que vous êtes sont ami et qu'il ne court aucun danger avec vous.  Soyez patient pour son éducation et vous serez surpris des plaisirs qu'il vous apportera.</p></i>
</div>
<div class="pricing-tables pure-g-r">
    <div class="pure-u-1-3">
        <div class="pricing-table pricing-table-free"> 
            <div class="pricing-table-header">
                <h2>Lapin-garou</h2>
                <span class="pricing-table-price">
                    158€
                </span>
            </div>
            <ul class="pricing-table-list">
                <li>Véritable lapin mythologique</li>
                <li>Pour proféssionel seulement</li>
                <li>Nous nous déchargons de toute résponsabilité quand aux actions de cet animal une fois chez vous ;)</li>
                <li>Livrée sans la cage ...</li>
            </ul>
            <button class="pure-button button-choose">Choose</button>
        </div>
    </div>
    <div class="pure-u-1-3">
        <div class="pricing-table pricing-table-biz pricing-table-selected">
            <div class="pricing-table-header">
                <h2>Lapin - Tigre </h2>

                <span class="pricing-table-price">
                    320€ <span>seulement pour un animal en voie de disparition</span>
                </span>
            </div>
            <ul class="pricing-table-list">
                <li>Nous vous recomendons d'utiliser les conseils spécifique à cette race</li>
            </ul>
            <button class="pure-button button-choose">Choose</button>
        </div>
    </div>
    <div class="pure-u-1-3">
        <div class="pricing-table pricing-table-enterprise">
            <div class="pricing-table-header">
                <h2>Kit de soins</h2>
                <span class="pricing-table-price">
                    45€ <span>Pour la panoplie complete</span>
                </span>
            </div>
            <ul class="pricing-table-list">
                <li>Brosse à dents 25cm</li>
                <li>Gel spécial rongeur</li>
                <li>Crème rajeunissante</li>
            </ul>
            <button class="pure-button button-choose">Choose</button>
        </div>
    </div>
</div> 
<div class="block">
    <div class="pure-u-1-3">
        <img id="cadeau" <?php echo $this->Html->image("cadeau.jpg");?></img>
    </div>
    <div class="pure-u-1-3">
        <div class="pricing-table pricing-table-enterprise offre">
            <div class="pricing-table-header color">
                <h2>Notre offre emballage cadeau</h2>
                <span class="pricing-table-price ">
                    5€ <span>Pour vous</span>
                </span>
            </div>
            <ul class="pricing-table-list">
                <li>Ne Ratez pas l'occasion de faire plaisir</li>
                <li>Pour les plus jeunes</li>
                <li>Mais aussi les plus vieux</li> 
                <button class="pure-button button-choose">Choose</button>
            </ul>
           
        </div>
    </div>
    </div>
</div>