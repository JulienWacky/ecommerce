<?php echo $this->element('ban_user'); ?>
    <div class="connexion">
        <h2>Inscription</h2>
        <?php echo $this->form->create('User', array('class' => "pure-form pure-form-stacked")); ?>
        <?php echo $this->form->input('username',array('label'=>"Login : ")); ?>
        <?php echo $this->form->input('mail',array('label'=>"Email : ")); ?>
        <?php echo $this->form->input('password',array('label'=>"Mot de passe : ")); ?>
        <?php echo $this->form->submit("Register", array('class'=>"pure-button buttonRose")); ?>
        <h2>
            <?php echo $this->Session->flash(); ?>
        </h2>
    </div>