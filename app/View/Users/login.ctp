<?php echo $this->element('ban_user'); ?>
    <div class="connexion">
    	<h2>Connexion</h2>
    	<?php echo $this->form->create('User', array('class' => "pure-form pure-form-stacked")); ?>
    	<?php echo $this->form->input('username',array('label'=>"Pseudo ")); ?>
    	<?php echo $this->form->input('password',array('label'=>"Mot de passe : ")); ?>
    	<?php echo $this->form->submit("Connexion",array('class'=>"pure-button buttonRose")); ?>
    </div>