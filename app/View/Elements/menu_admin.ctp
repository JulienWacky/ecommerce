    <div class="panel">

        <div class="menuadmin">
    	<div class="pure-menu pure-menu-open adminmenu">
    		<a class="pure-menu-heading"><b>PANEL ADMIN</b></a>

    		<ul>
    			
    			<li class="pure-menu-heading"><?php echo $this->Html->link(__('GESTION CATEGORIE'), array('controller' => 'categories', 'action' => 'panel_categorie')); ?></li>
                <li><?php echo $this->Html->link(__('► Ajouter une categorie'), array('controller' => 'categories', 'action' => 'add_cate')); ?></li>
                <li><?php echo $this->Html->link(__('► Ajouter sous-categorie'), array('controller' => 'categories', 'action' => 'add')); ?></li>
    			<li class="pure-menu-heading"><?php echo $this->Html->link(__('GESTION de Produits'), array('controller' => 'products', 'action' => 'panel_produit')); ?></li>
                <li>    <?php echo $this->Html->link(__('► Ajouter un produit'), array('controller' => 'products', 'action' => 'add')); ?></li>
    			<li class="pure-menu-heading"><?php echo $this->Html->link(__('Gestion Utilisateurs'), array('controller' => 'users', 'action' => 'panel_user')); ?></li>
                    <li class="pure-menu-heading"><?php echo $this->Html->link(__('Gestion Blog'), array('controller' => 'posts', 'action' => 'panel_blog')); ?></li>
                    <li> <?php echo $this->Html->link('► Ajouter une categorie', array('action' => 'add', 'controller' => 'bcategories')); ?></li>
                    <li><?php echo $this->Html->link('► Ajouter un article', array('action' => 'add',  'controller' => 'posts')); ?></li>
    		</ul>

    	</div>
    </div>



<!-- 
</div> -->