<?php
$Description = __d('cake_dev', 'eRabbit');
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $Description ?>:
		<?php echo $title_for_layout; ?>	
	</title>
	<?php
		echo $this->Html->meta('icon');
		echo $scripts_for_layout;
		echo $this->Html->css('pricing.css');
		echo $this->Html->css('style.css');
		echo $this->Html->css('slide.css');
    echo $this->Html->css('main-grid.css');
    echo $this->Html->css('blog.css');
		echo $this->Html->css('menu');
		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('jquery.glide');
		echo $this->Html->script('scriptpanier')
	?>
	<link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css">
	<script src="http://yui.yahooapis.com/3.14.0/build/yui/yui-min.js"></script>
	<script src="http://code.jquery.com/jquery-1.11.0.min.js"></script>
  <script src="../js/jquery.glide.js"></script> 
</head>
<body>
	<script>
            YUI({
                classNamePrefix: 'pure'
            }).use('gallery-sm-menu', function (Y) {

                var horizontalMenu = new Y.Menu({
                    container         : '.demo-horizontal-menu',
                    sourceNode        : '#std-menu-items',
                    orientation       : 'horizontal',
                    hideOnOutsideClick: false,
                    hideOnClick       : false
                });

                horizontalMenu.render();
                horizontalMenu.show();

            });
        </script>
        <?php
        echo $this->Session->flash();
        ?>
    		<div id="container">
    			<div class="demo-horizontal-menu pure-menu pure-menu-open pure-menu-horizontal pure-menu-fixed">
    				<a class="pure-menu-heading" href="/"><img id="logo" <?php echo $this->Html->image("logo.png", array('alt' => 'logo', 'width' => '110px'));?></img> </a>
				<ul id="std-menu-items">
	                <li><?php echo $this->html->link("Mon Lapin", array('controller' => 'categories', 'action' => 'view', 12)); ?>
	                <?php echo "</li>"?>
	                <li>
	                	<a href="#">Accessoires</a>
	                	<ul>
      	              <li><?php echo $this->html->link("Jouets", array('controller' => 'categories', 'action' => 'view', 2)); ?></li>
                      <li><?php echo $this->html->link("Soins", array('controller' => 'categories', 'action' => 'view', 13)); ?></li>
                      <li><?php echo $this->html->link("Nourriture", array('controller' => 'categories', 'action' => 'view', 11)); ?></li>
                      <li><?php echo $this->html->link("Utilitaires", array('controller' => 'categories', 'action' => 'view', 14)); ?></li>
	                	</ul>
	                </li>
	                <?php echo "<li>"?>
	                <?php echo $this->html->link("Contact", array('action' => 'index', 'controller' => 'contact')); ?>
	                <?php echo "</li>"?>
                  <?php echo "<li>"?>
                  <?php echo $this->html->link("Conseil", array('action' => 'index', 'controller' => 'posts')); ?>
                  <?php echo "</li>"?>
				
				          </ul>
				          <?php if(AuthComponent::user('id')): ?>
          				<div style="float:right;float:right;margin-top:15px;margin-right:15px;"><?php echo $this->html->link("Deconnexion", array('action' => 'logout', 'controller' => 'users')); ?></div>
          				  <a class="pure-menu-heading" style="float:right;margin-top:5px;" href="/users/edit" ><img  <?php echo $this->Html->image("user.png", array('action' => 'panel', 'controller' => 'categories'));?> </a>
                  <?php else: ?>
                  <div style="float:right;float:right;margin-top:15px;margin-right:5px;"><?php echo $this->html->link("Login", array('action' => 'login', 'controller' => 'users')); ?>  </div>
                        <div style="float:right;float:right;margin-top:15px;margin-right:0px;"><?php echo $this->html->link("Register", array('action' => 'signup', 'controller' => 'users')); ?></div>
            				<?php endif; ?>

          				<?php if(AuthComponent::user('role_id') == '2'): ?>
          				    <a class="pure-menu-heading" style="float:right;margin-top:5px;" href="/categories/panel_categorie" ><img  <?php echo $this->Html->image("panel.png", array('action' => 'panel_categorie', 'controller' => 'categories'));?> </a>
          			   <?php endif; ?>

			           <div style="float:right;margin-top:5px;">

                  <?php 
                  $cart = $this->Session->read('Cart');
                  if($cart == NULL) { 
                          echo $this->Html->image('cart.png', array('alt' => "panier", "id" => "go", 'class'=>'pure-menu-heading', 'style'=>"float:right;margin-top:5px;"));
                          $cart = array();
                         }else{
                          echo $this->Html->image('cart2.png', array('alt' => "panier", "id" => "go", 'style'=>"float:right;margin-top:5px;"));
                         } 
                  ?>
     		       </div>
            <form class="pure-form" style="float:right;margin-top:15px;margin-right:25px;">
              <input type="text" class="pure-input-rounded">
              <button type="submit" class="pure-button">Search</button>
        		</form>
			   </div>
       </div>
    		<div id="thepanier">
        <h1>Mon panier</h1>
        <table class="pure-table">
           <thead>
            <tr>
                <th class="thProduit">Produit</th>
                <th class="thPrix">Prix</th>
                <th class="thQuantite">Quantité</th>
                <th class="thAction">Action</th>
            </tr>
           </thead>
            <?php $totalPrice = 0; ?>
            <?php foreach ($cart as $key => $product): ?>
            <tr>
              <td>
                <?php  echo $this->Html->link($product['Product']['name'], array('action'=> 'view', $product['Product']['id'])); ?>
              </td>
              <td>
                <?php echo $product['Product']['price']; ?>
              </td>
              <td>
                <?php echo $product['Product']['quantity'];?>
              </td>
              <td>
                <?php  echo $this->Html->link('Supprimer', array('action' => 'delete_cart',$key)); ?>
              </td>
            </tr>
            <?php $totalPrice = $totalPrice  + ($product['Product']['price'] * $product['Product']['quantity'])."€"; ?>
            <?php endforeach; ?>
            <tr>
                <th>Prix Total: </th>
                <th><?php echo $totalPrice; ?>
                </th>
                <th></th>
                <th>
                  <?php echo $this->Html->link('Vider', array('controller'=>'Products', 'action'=>'empty_cart'));?>
                </th>
            </tr>
        </table>
          <?php echo $this->Html->link('Voir le panier', array('controller'=>'Products', 'action'=>'cart')); ?>
      </div>
    </div>
	</div>
	<div id="content">
		<?php echo $this->Session->flash(); ?>
		<?php echo $this->fetch('content'); ?>
	</div>
</div>
<div class="footer">
				<div class="pure-menu pure-menu-horizontal pure-menu-open toto">
          <ul>
            <li><a href="#">Copyright 2018 | </a></li>
            <li><?php echo $this->html->link("Contact", array('action' => 'index', 'controller' => 'contact')); ?></li>
            <li><a href="http://t2.gstatic.com/images?q=tbn:ANd9GcTyXKxnSh67TuaiRfhv1cF6y7_z8mnMQGvEJAuLpKEHHw7eLOBfeVT46P0TvQ">| By eRabbit Corporation | </a></li>
            <li><a href="https://bitbucket.org/JulienWacky/ecommerce/src">Bitbucket</a></li>   
          </ul>   
        </div>
</div>
	<?php echo $this->Html->script('script');
			echo $this->Html->script('jQuery');   ?>
</body>
</html>