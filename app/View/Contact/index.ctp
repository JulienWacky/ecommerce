<?php echo $this->element('ban_user'); ?>
<?php

$this->set('title_for_layout', "Nous contacter");

?>
<div id="contact" class="connexion">
	<h1>Nous contacter</h1>

	<?php echo $this->Form->create('Contact', array('class' => "pure-form pure-form-stacked")); ?>
	<?php echo $this->Form->input('name', array('label' => 'Votre nom', 'required')); ?>
	<?php echo $this->Form->input('email', array('label' => 'Votre email', 'type' => 'email', 'required')); ?>
	<?php echo $this->Form->input('content', array('label' => 'Votre message', 'type' => 'textarea','required')); ?>
	<?php echo $this->Form->submit('Register', array('class'=>"pure-button buttonRose")); ?>
</div>