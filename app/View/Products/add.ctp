<?php echo $this->element('ban_admin'); ?>
        <?php echo $this->element('menu_admin'); ?>
    <div class="products blockadmin">
    	<?php echo $this->Form->create('Product', array('class' => "pure-form pure-form-stacked")); ?>
    		<h2>Ajout Produit</h2>
    		<?php
    		echo $this->Form->input('category_id');
    		echo $this->Form->input('name');
    		echo $this->Form->input('slug');
    		echo $this->Form->input('description');
    		echo $this->Form->input('image');
    		echo $this->Form->input('price');
    		echo $this->Form->input('weight');
    		?>
            <br>
    	<?php echo $this->Form->end(__('Submit')); ?>
    </div>
</div>