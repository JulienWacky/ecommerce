<?php echo $this->element('ban_user'); ?>
<div class="fiche">
	<div class="products_view">

		<h2><?php echo "►".h($product['Product']['name']); ?></h2>
		<br>
		<dl>	
			<dd id="lapimg">
				<?php echo $this->html->image(h($product['Product']['image'])); ?>
				&nbsp;
			</dd>
			<h3><?php echo __('Description'); ?></h3>
			<dd>
				<?php echo h($product['Product']['description']); ?>
				&nbsp;
			</dd>
			<br>
			
			<h3><?php echo __('Poids'); ?></h3>
			<dd>
				<?php echo h($product['Product']['weight'])." KG"; ?>
				&nbsp;
			</dd>
			<dd class="price2">
				<?php echo h($product['Product']['price'])."€"; ?>
				&nbsp;
			</dd>
			<br>
			<dd class="add">
				<?php echo "+ ".$this->Html->link('Ajouter a mon panier', array('action' => 'add_to_cart',  $product['Product']['id']));?>
				&nbsp;
			</dd>
		</dl>

<div class="comment">
		<h2 id="goAvis">Avis ></h2>
		<div id="avis">
		<?php

		foreach($a['Avisproduit'] as $c)
			{
				echo '<strong>'.$c['pseudo'].'</strong>';
				echo '<p>'.$c['contenu'].'</p>';
			} ?>
		</div>
		<h2 id="goEditAvis">Ajouter un avis ></h2>
		<div id="editAvis">
			<?php 
			echo $this->Form->create('Avisproduit', array('url'=>array('controller' => 'products', 'action' => 'view', $a['Product']['id']), 'class' => "pure-form pure-form-stacked"));
			echo $this->Form->input('pseudo', array('label'=>"Pseudo : "));
			echo $this->Form->input('mail', array('label'=>"Email : "));
			echo $this->Form->input('contenu', array('label'=>"Votre Avis : "));
			echo $this->Form->input('product_id', array('type'=>'hidden','value' => $a['Product']['id']));
			?>
			<div class="pure-button">
			<?php
			echo $this->Form->submit('Envoyer', array('type'=>'submit', 'class'=>'pure-button'));
			?>
		</div>
	</div>
</div>
</div>

