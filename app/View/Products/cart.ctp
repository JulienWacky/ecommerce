<?php echo $this->element('ban_user'); ?>
    <h1 class="flash">
      <?php echo $this->Session->flash();?>
      <br><br>
    </h1>
    <div class="containerCart">
      <div class="cart">
        <h1>Mon panier</h1>
        <table class="pure-table">
         <thead>
          <tr>
            <th class="thProduit">Produit</th>
            <th class="thPrix">Prix</th>
            <th class="thQuantite">Quantité</th>
            <th class="thAction">Action</th>
          </tr>
        </thead>
        <?php $totalPrice = 0; ?>
        <?php foreach ($cart as $key => $product): ?>
        <tr>
          <td>
            <?php echo $this->Html->link($product['Product']['name'], array('action'=> 'view', $product['Product']['id'])); ?>
          </td>
          <td>
            <?php echo $product['Product']['price']; ?>
          </td>
          <td>
            <?php
              echo $product['Product']['quantity'];
            ?>
          </td>
          <td>
            <?php echo $this->Html->link('Supprimer', array('action' => 'delete_cart',$key)); ?>
          </td>
        </tr>
        <?php $totalPrice = $totalPrice  + ($product['Product']['price'] * $product['Product']['quantity'])."€"; ?>
      <?php endforeach; ?>
      <tr>
        <th>Prix Total: </th>
        <th><?php echo $totalPrice; ?>
      </th>
      <th></th>
      <th>
        <?php
        echo $this->Html->link('Vider', array('action'=>'empty_cart'));
        ?>
      </th>
    </tr>
  </table>
   <div class="retourIndex">
    <h3><?php echo $this->Html->link("J'adopte !", array('controller'=>'Orders', 'action' => 'index')); ?></h3>
   <!--  <?php echo $this->Html->link('Retour index', array('controller'=>'Pages', 'action' => 'display')); ?> -->
  </div><br><br><br>
</div>
</div>