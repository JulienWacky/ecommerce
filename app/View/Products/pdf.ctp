<?php
App::import('Vendor','html2pdf/html2pdf');
try{
    $pdf = new HTML2PDF('P','A4','fr');
    $pdf->pdf->SetDisplayMode('fullpage');
    $pdf->writeHTML($content);
    $pdf->Output('test.pdf');
}
catch(HTML2PDF_exception $e){
    die($e);
}
?>