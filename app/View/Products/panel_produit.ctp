
<?php echo $this->element('ban_admin'); ?>
<?php echo $this->element('menu_admin'); ?>
 	<h2> GESTION PRODUITS</h2>
 	<table class="pure-table pure-table-bordered" style="float:left; width: 1000px;margin-right:100px;margin-top:20px;">
       
 		<tr> <thead>
 			<th><?php echo __('Name'); ?></th>
 			<!-- 		<th><?php echo __('Description'); ?></th> -->
 			<!-- <th><?php echo __('Image'); ?></th> -->
 			<th><?php echo __('Price'); ?></th>
 			<th><?php echo __('Weight'); ?></th>
<!--  			<th><?php echo __('Views'); ?></th> -->
 			<th class="actions"><?php echo __('Actions'); ?></th>
        </thead>
 		</tr>

 		<?php foreach ($products as $product): ?>
 		<tr>

 			<td><?php echo($product['Product']['name']); ?></td>
 			<!-- <td><?php echo $product['Product']['description']; ?></td> -->
 	<!-- 		<td><?php echo $this->html->image( $product['Product']['image'], array( 'class' => 'panel_img')); ?></td> -->
 			<td><?php echo $product['Product']['price']; ?></td>
 			<td><?php echo $product['Product']['weight']; ?></td>
<!--  			<td><?php echo $product['Product']['views']; ?></td> -->

 			<td class="actions">
 				<?php echo $this->Html->link(__('+'), array('controller' =>'Products','action' => 'view', $product['Product']['id'])); ?> 
 				<?php echo $this->Html->link(__('Editer'), array('controller' =>'Products','action' => 'edit', $product['Product']['id'])); ?> 
 				<?php echo $this->Form->postLink(__('Suprimer'), array('controller' =>'Products', 'action' => 'delete', $product['Product']['id']), null, __('Are you sure you want to delete # %s?', $product['Product']['id'])); ?> 
 			</td>
 			
 		</tr>
 	<?php endforeach; ?>
 </table>
</div>