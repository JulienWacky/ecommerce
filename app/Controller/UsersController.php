<?php

class UsersController extends AppController{



	function signup(){
		if($this->request->is('post')){
			$d = $this->request->data;

			$d['User']['id'] = null;
			/*$d['User']['lastlogin'] = '2009-08-08 12:17:00';*/

			if(!empty($d['User']['password'])){
				$d['User']['password'] = Security::hash($d['User']['password'], null, true);
			}
			/*debug($d);*/
			if($this->User->save($d,true,array('username','password','mail','role_id'))){
				$this->Session->setFlash("Votre compte a bien été crée","notif");
				$this->redirect('/users/login');

			}else{
				$this->Session->setFlash("Merci de corriger vos erreurs","notif", array('type' => 'error'));
				
			}
		}
	}

	function logout(){
		$this->Auth->logout();
		$this->redirect($this->referer());
	}

	function login(){
		if($this->request->is('post')){
			if($this->Auth->login()){
				$this->User->id = $this->Auth->user("id");
				$this->User->saveField('lastlogin', date('Y-a-d H:i:s'));
				$this->Session->setFlash("Vous etes maintenant connecté","notif");
				$this->redirect('/');
			}
			else{
				$this->Session->setFlash("Identifiants incorrects","notif", array('type' => 'error')); 
			}
		}
	}

	function edit(){
		$user_id = $this->Auth->user('id');
		if(!$user_id){
			$this->redirect('/');
			die();
		}

		$this->User->id = $user_id;
		$passError = false;

		if($this->request->is('put') || $this->request->is('post')){

			$d = $this->request->data;
			$d['User']['id'] = $user_id;

			if(!empty($d['User']['pass1'])){
				if($d['User']['pass1']==$d['User']['pass2']){
					$d['User']['password'] = Security::hash($d['User']['pass1'], null, true);
				}else{
					$passError = true;
				}
			}

			if($this->User->save($d, true, array('nom','prenom','adresse','tel','username','password'))){
				$this->Session->setFlash("Votre profil a été edité","notif");
			}else{
				$this->Session->setFlash("Impossible de sauvegarder","notif", array('type' => 'error'));
			}
			if($passError) $this->User->validationErrors['pass2'] = array('Les mots de passe ne correspondent pas');
		}else{
			$this->request->data = $this->User->read();
		}
	}




	/////////////////////ADMIN////////////////////////////

	public function panel_user() {
		if(AuthComponent::user('role_id') == '2'){
			/*$this->loadModel('User');*/
			$users = $this->User->find('all');
			$this->User->recursive = 0;
			$this->set('users', $users);
		}
		else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));

		}
	}



	function admin_index()
	{
		$users = $this->Users->findAll();
		$this->set('users', $allusers);
	}

}
