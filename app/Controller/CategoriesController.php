<?php
App::uses('AppController', 'Controller');

class CategoriesController extends AppController {

	public $components = array('Paginator');

	public function index() {
		$this->Category->recursive = 0;
		$this->set('categories', $this->Paginator->paginate());
	}


	

	public function view($id = null) {
		if (!$this->Category->exists($id)) {
			throw new NotFoundException(__('Invalid category'));
		}
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->set('category', $this->Category->find('first', $options));

	}



/////////////////ADMIN/////////////////////////

	public function add() {
		if(AuthComponent::user('role_id') == '2'){
			if ($this->request->is('post')) {
				$this->Category->create();
				if ($this->Category->save($this->request->data)) {
					/*return $this->flash(__('The category has been saved.'), array('action' => 'index'));*/
					return $this->redirect(array('controller'=> 'categories', 'action' => 'panel_categorie'));
				}
			}
			$parentCategories = $this->Category->ParentCategory->find('list');
			$this->set(compact('parentCategories'));
		}else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));
		}
	}
		public function add_cate() {
		if(AuthComponent::user('role_id') == '2'){
			if ($this->request->is('post')) {
				$this->Category->create();
				if ($this->Category->save($this->request->data)) {
					/*return $this->flash(__('The category has been saved.'), array('action' => 'index'));*/
					return $this->redirect(array('controller'=> 'categories', 'action' => 'panel_categorie'));
				}
			}
			$parentCategories = $this->Category->ParentCategory->find('list');
			$this->set(compact('parentCategories'));
		}else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));
		}
	}

	public function panel_categorie() {
		if(AuthComponent::user('role_id') == '2'){

			$this->Category->recursive = 0;
			$this->set('categories', $this->Paginator->paginate());
		}
		else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));

		}
	}


	public function edit($id = null) {
		if(AuthComponent::user('role_id') == '2'){
			if (!$this->Category->exists($id)) {
				throw new NotFoundException(__('Invalid category'));
			}
			if ($this->request->is(array('post', 'put'))) {
				if ($this->Category->save($this->request->data)) {
					return $this->flash(__('The category has been saved.'), array('action' => 'index'));
					$this->redirect(array('controller'=> 'categories', 'action' => 'panel'));
				}
			} else {
				$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
				$this->request->data = $this->Category->find('first', $options);
			}
			$parentCategories = $this->Category->ParentCategory->find('list');
			$this->set(compact('parentCategories'));}
			else{
				$this->redirect(array('controller'=> 'pages', 'action' => '404'));
			}
		}

		public function delete($id = null) {
			if(AuthComponent::user('role_id') == '2'){
				$this->Category->id = $id;
				if (!$this->Category->exists()) {
					throw new NotFoundException(__('Invalid category'));
				}
				$this->request->onlyAllow('post', 'delete');
				if ($this->Category->delete()) {
					/*return $this->flash(__('The category has been deleted.'), array('action' => 'index'));*/
					return $this->redirect(array('controller'=> 'categories', 'action' => 'panel_categorie'));
				} else {
					return $this->flash(__('The category could not be deleted. Please, try again.'), array('action' => 'index'));
					return $this->redirect(array('controller'=> 'categories', 'action' => 'panel_categorie'));
				}
			}
			else{
				$this->redirect(array('controller'=> 'pages', 'action' => '404'));
			}
		}


/*public function membres_index() {
	$this->Category->recursive = 0;
	$this->set('categories', $this->Paginator->paginate());
}

public function membres_view($id = null) {
	if (!$this->Category->exists($id)) {
		throw new NotFoundException(__('Invalid category'));
	}
	$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
	$this->set('category', $this->Category->find('first', $options));
}

public function membres_add() {
	if ($this->request->is('post')) {
		$this->Category->create();
		if ($this->Category->save($this->request->data)) {
			return $this->flash(__('The category has been saved.'), array('action' => 'index'));
		}
	}
	$parentCategories = $this->Category->ParentCategory->find('list');
	$this->set(compact('parentCategories'));
}

public function membres_edit($id = null) {
	if (!$this->Category->exists($id)) {
		throw new NotFoundException(__('Invalid category'));
	}
	if ($this->request->is(array('post', 'put'))) {
		if ($this->Category->save($this->request->data)) {
			return $this->flash(__('The category has been saved.'), array('action' => 'index'));
		}
	} else {
		$options = array('conditions' => array('Category.' . $this->Category->primaryKey => $id));
		$this->request->data = $this->Category->find('first', $options);
	}
	$parentCategories = $this->Category->ParentCategory->find('list');
	$this->set(compact('parentCategories'));
}

public function membres_delete($id = null) {
	$this->Category->id = $id;
	if (!$this->Category->exists()) {
		throw new NotFoundException(__('Invalid category'));
	}
	$this->request->onlyAllow('post', 'delete');
	if ($this->Category->delete()) {
		return $this->flash(__('The category has been deleted.'), array('action' => 'index'));
		$this->redirect('/');
	} else {
		return $this->flash(__('The category could not be deleted. Please, try again.'), array('action' => 'index'));
		$this->redirect('/');
	}
}*/


}
