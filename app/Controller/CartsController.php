<?php
App::uses('AppController', 'Controller');
/**
 * Carts Controller
 *
 * @property Cart $Cart
 * @property PaginatorComponent $Paginator
 * @property adminComponent $admin
 */

class CartsController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

/**
 * index method
 *
 * @return void
 */
	public function index() {
		$this->Cart->recursive = 0;
		$this->set('carts', $this->Paginator->paginate());
	}

/**
 * view method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function view($id = null) {
		if (!$this->Cart->exists($id)) {
			throw new NotFoundException(__('Invalid cart'));
		}
		$options = array('conditions' => array('Cart.' . $this->Cart->primaryKey => $id));
		$this->set('cart', $this->Cart->find('first', $options));
	}

/**
 * add method
 *
 * @return void
 */
	public function add() {
		if ($this->request->is('post')) {
			$this->Cart->create();
			if ($this->Cart->save($this->request->data)) {
				return $this->flash(__('The cart has been saved.'), array('action' => 'index'));
			}
		}
		$products = $this->Cart->Product->find('list');
		$this->set(compact('products'));
	}

/**
 * edit method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function edit($id = null) {
		if (!$this->Cart->exists($id)) {
			throw new NotFoundException(__('Invalid cart'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cart->save($this->request->data)) {
				return $this->flash(__('The cart has been saved.'), array('action' => 'index'));
			}
		} else {
			$options = array('conditions' => array('Cart.' . $this->Cart->primaryKey => $id));
			$this->request->data = $this->Cart->find('first', $options);
		}
		$products = $this->Cart->Product->find('list');
		$this->set(compact('products'));
	}

/**
 * delete method
 *
 * @throws NotFoundException
 * @param string $id
 * @return void
 */
	public function delete($id = null) {
		$this->Cart->id = $id;
		if (!$this->Cart->exists()) {
			throw new NotFoundException(__('Invalid cart'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cart->delete()) {
			return $this->flash(__('The cart has been deleted.'), array('action' => 'index'));
		} else {
			return $this->flash(__('The cart could not be deleted. Please, try again.'), array('action' => 'index'));
		}
	}

/**
 * membres_index method
 *
 * @return void
 */
// 	public function membres_index() {
// 		$this->Cart->recursive = 0;
// 		$this->set('carts', $this->Paginator->paginate());
// 	}

// /**
//  * membres_view method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function membres_view($id = null) {
// 		if (!$this->Cart->exists($id)) {
// 			throw new NotFoundException(__('Invalid cart'));
// 		}
// 		$options = array('conditions' => array('Cart.' . $this->Cart->primaryKey => $id));
// 		$this->set('cart', $this->Cart->find('first', $options));
// 	}

// /**
//  * membres_add method
//  *
//  * @return void
//  */
// 	public function membres_add() {
// 		if ($this->request->is('post')) {
// 			$this->Cart->create();
// 			if ($this->Cart->save($this->request->data)) {
// 				return $this->flash(__('The cart has been saved.'), array('action' => 'index'));
// 			}
// 		}
// 		$products = $this->Cart->Product->find('list');
// 		$this->set(compact('products'));
// 	}

// /**
//  * membres_edit method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function membres_edit($id = null) {
// 		if (!$this->Cart->exists($id)) {
// 			throw new NotFoundException(__('Invalid cart'));
// 		}
// 		if ($this->request->is(array('post', 'put'))) {
// 			if ($this->Cart->save($this->request->data)) {
// 				return $this->flash(__('The cart has been saved.'), array('action' => 'index'));
// 			}
// 		} else {
// 			$options = array('conditions' => array('Cart.' . $this->Cart->primaryKey => $id));
// 			$this->request->data = $this->Cart->find('first', $options);
// 		}
// 		$products = $this->Cart->Product->find('list');
// 		$this->set(compact('products'));
// 	}

// /**
//  * membres_delete method
//  *
//  * @throws NotFoundException
//  * @param string $id
//  * @return void
//  */
// 	public function membres_delete($id = null) {
// 		$this->Cart->id = $id;
// 		if (!$this->Cart->exists()) {
// 			throw new NotFoundException(__('Invalid cart'));
// 		}
// 		$this->request->onlyAllow('post', 'delete');
// 		if ($this->Cart->delete()) {
// 			return $this->flash(__('The cart has been deleted.'), array('action' => 'index'));
// 		} else {
// 			return $this->flash(__('The cart could not be deleted. Please, try again.'), array('action' => 'index'));
// 		}
//	}
}
