<?php

class PostsController extends AppController
{

	var $name = "Posts";
	var $paginate = array(
		'Post' => array(
			'limit' => 10,
			'order' => array(
				'Post.date' => 'Desc'
			)
		));
	var $uses = array('Post', 'Comment');

	function index()
	{
		$q = $this->paginate('Post');
		$this->set('articles', $q);
	}

	function category($id)
	{
		$q = $this->paginate('Post', array("Post.bcategory_id" => $id));
		$this->set('articles',$q);
		$this->render("index");
	}

	function voir($id)
	{
		if(!empty($this->data))
		{
			if($this->Comment->validates($this->data))
			{
			$this->Session->setFlash('Votre commentaire à bien été posté');
			$this->Comment->save($this->data);
			}
			else
			{
				$this->Session->setFlash('Merci de valider vos champs');
				$this->validateErrors($this->Comment);
			}
		}
		$options = array('conditions' => array('Post.' . $this->Post->primaryKey => $id));
		$q = $this->Post->find('first', $options);
		$this->set('a', $q);
	}


	function edit($id=null)
	{
		if(AuthComponent::user('role_id') == '2'){
		$c = $this->Post->Bcategory->find('list', array(
			'recursive' => -1,
			'fields' => array('id','titre')
			));
		$this->set('cats', $c);
		/*$this->layout="admin";*/
		
		if($this->request->is('get'))
		{
			if(isset($id))
			{
			$this->Post->id = $id;
			$this->data = $this->Post->read();
			}
		}
		if($this->request->is('post'))
		{
			$this->Post->save($this->data);
			$this->redirect(array('action'=>'panel_blog'));
		}
	}else{
		$this->redirect(array('controller'=> 'pages', 'action' => '404'));
	}


	}


		function add($id=null)
	{
		if(AuthComponent::user('role_id') == '2'){
		$c = $this->Post->Bcategory->find('list', array(
			'recursive' => -1,
			'fields' => array('id','titre')
			));
		$this->set('cats', $c);
		/*$this->layout="admin";*/
		
		if($this->request->is('get'))
		{
			if(isset($id))
			{
			$this->Post->id = $id;
			$this->data = $this->Post->read();
			}
		}
		if($this->request->is('post'))
		{
			$this->Post->save($this->data);
			$this->redirect(array('action'=>'alist'));
		}
	}else{
		$this->redirect(array('controller'=> 'pages', 'action' => '404'));
	}
	

	}

	function delete($id)
	{
		$this->Post->delete($id);
		$this->Session->setFlash('Article bien supprimé');
		$this->redirect('alist');
	}

	function panel_blog(){
		if(AuthComponent::user('role_id') == '2'){
			$q = $this->paginate('Post');
		$this->set('articles', $q);
		}
		else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));

		}
	}

}

?>