<?php
App::uses('AppController', 'Controller');

class ProductsController extends AppController {

	var $name = "products";
	var $uses = array('Product', 'Avisproduit');

	public $components = array('Paginator');

	public function index() {
		$this->Product->recursive = 0;
		$this->set('products', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->Product->exists($id)) {
			throw new NotFoundException(__('Le produit n\'existe pas'));
		}
		$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
		$this->set('product', $this->Product->find('first', $options));


		if(!empty($this->data))
		{
			if($this->Avisproduit->validates($this->data))
			{
				$this->Session->setFlash('Votre commentaire à bien été posté');
				$this->Avisproduit->save($this->data);
			}
			else
			{
				$this->Session->setFlash('Merci de valider vos champs');
				$this->validateErrors($this->Avisproduit);
			}
		}
		$q = $this->Product->find('first', array(
			'conditions' => array('Product.id' => $id)
			));
		$this->set('a', $q);
	}




	/////////////////////////////////////////ADMIN///////////////////////////////

	public function panel_produit() {
		if(AuthComponent::user('role_id') == '2'){
			$products = $this->Product->find('all');
			/*var_dump($products);*/
			$this->Product->recursive = 0;
			$this->set('products', $products);
		}
		else{
			$this->redirect(array('controller'=> 'pages', 'action' => '404'));

		}
	}

	public function add() {
		if(AuthComponent::user('role_id') == '2'){
			if ($this->request->is('post')) {
				$this->Product->create();
				if ($this->Product->save($this->request->data)) {
					$this->Session->setFlash(__('Produit sauvegardé.'));
					return $this->redirect(array('action' => 'panel_produit'));
				} else {
					$this->Session->setFlash(__('Une erreur est survenue. Recommencez'));
				}
			}
			$categories = $this->Product->Category->find('list');
		// $brands = $this->Product->Brand->find('list');
			$this->set(compact('categories', 'brands'));}
			else{
				$this->redirect(array('controller'=> 'pages', 'action' => '404'));
			}
		}

		public function edit($id = null) {
			if(AuthComponent::user('role_id') == '2'){
				if (!$this->Product->exists($id)) {
					throw new NotFoundException(__('Le produit n\'existe pas'));
				}
				if ($this->request->is(array('post', 'put'))) {
					if ($this->Product->save($this->request->data)) {
						$this->Session->setFlash(__('Produit sauvegardé.'));
						return $this->redirect(array('controller'=> 'products', 'action' => 'panel_produit'));
					} else {
						$this->Session->setFlash(__('Une erreur est survenue. Recommencez'));
					}
				} else {
					$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
					$this->request->data = $this->Product->find('first', $options);
				}
				$categories = $this->Product->Category->find('list');
				$this->set(compact('categories', 'brands'));}
				else{
					$this->redirect(array('controller'=> 'pages', 'action' => '404'));
				}
			}

			public function delete($id = null) {
				if(AuthComponent::user('role_id') == '2'){
					$this->Product->id = $id;
					if (!$this->Product->exists()) {
						throw new NotFoundException(__('Le produit n\'existe pas'));
					}
					$this->request->onlyAllow('post', 'delete');
					if ($this->Product->delete()) {
						$this->Session->setFlash(__('Produit supprimé.'));
					} else {
						$this->Session->setFlash(__('Une erreur est survenue. Recommencez'));
					}
					return $this->redirect(array('controller'=> 'products', 'action' => 'panel_produit'));}
					else{
						$this->redirect(array('controller'=> 'pages', 'action' => '404'));
					}
				}

				public function admin_index() {
					$this->Product->recursive = 0;
					$this->set('products', $this->Paginator->paginate());
				}

				public function admin_view($id = null) {
					if (!$this->Product->exists($id)) {
						throw new NotFoundException(__('Invalid product'));
					}
					$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
					$this->set('product', $this->Product->find('first', $options));
				}

				public function admin_add() {
					if ($this->request->is('post')) {
						$this->Product->create();
						if ($this->Product->save($this->request->data)) {
							$this->Session->setFlash(__('The product has been saved.'));
							return $this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
						}
					}
					$categories = $this->Product->Category->find('list');
					$brands = $this->Product->Brand->find('list');
					$this->set(compact('categories', 'brands'));
				}

				public function admin_edit($id = null) {
					if (!$this->Product->exists($id)) {
						throw new NotFoundException(__('Invalid product'));
					}
					if ($this->request->is(array('post', 'put'))) {
						if ($this->Product->save($this->request->data)) {
							$this->Session->setFlash(__('The product has been saved.'));
							return $this->redirect(array('action' => 'index'));
						} else {
							$this->Session->setFlash(__('The product could not be saved. Please, try again.'));
						}
					} else {
						$options = array('conditions' => array('Product.' . $this->Product->primaryKey => $id));
						$this->request->data = $this->Product->find('first', $options);
					}
					$categories = $this->Product->Category->find('list');
					$brands = $this->Product->Brand->find('list');
					$this->set(compact('categories', 'brands'));
				}

				public function admin_delete($id = null) {
					$this->Product->id = $id;
					if (!$this->Product->exists()) {
						throw new NotFoundException(__('Invalid product'));
					}
					$this->request->onlyAllow('post', 'delete');
					if ($this->Product->delete()) {
						$this->Session->setFlash(__('The product has been deleted.'));
					} else {
						$this->Session->setFlash(__('The product could not be deleted. Please, try again.'));
					}
					return $this->redirect(array('action' => 'index'));
				}





	///////////////////////PANIER///////////////////////////////////
				
				public function add_to_cart($id = null) {
					$this->Product->id = $id;
        //verif produit db
					if (!$this->Product->exists()) {
						throw new NotFoundException(__('Produit non-existant'));
					}

        //Verif cart est dans panier
					$productsInCart = $this->Session->read('Cart');
					$alreadyIn = false;
					/*var_dump($productsInCart); die();*/
					foreach ($productsInCart as $key => $productInCart) {
						if ($productInCart['Product']['id'] == $id) {
							$productsInCart[$key]['Product']['quantity']++;
							$this->Session->write('Cart', $productsInCart);
							$alreadyIn = true;
						}
					}

        //si produit n'est pas dans le panier
					if (!$alreadyIn) {
						$amount = count($productsInCart);
						$this->Session->write('Cart.' . $amount, $this->Product->read(null, $id));
						$this->Session->write('Counter', $amount + 1);
						$this->Session->setFlash(__('Votre produit a bien été ajouté'));
					} else {
						$this->Session->setFlash(__('Produit mis a jour'));

					}


					$this->redirect(array('controller' => 'products', 'action' => 'cart'));
				}


				public function delete_cart($id = null) {
					if (is_null($id)) {
						throw new NotFoundException(__l('Votre panier est vide'));
					}
        //delete product from cart
					if ($this->Session->delete('Cart.' . $id)) {
            //Trie
						$cart = $this->Session->read('Cart');
						sort($cart);
						$this->Session->write('Cart', $cart);
            //Update compteur
						$this->Session->write('Counter', count($cart));
						$this->Session->setFlash('Produit supprimé');
					}
					return $this->redirect(array('action' => 'cart'));
				}

				public function cart() {
        			//List des produits dans le panier
					$cart = array();

					if ($this->Session->check('Cart')) {
						$cart = $this->Session->read('Cart');
					}

					$this->set(compact('cart'));
				}

				public function empty_cart() {
        			//Vider panier
					$this->Session->delete('Cart');
					$this->Session->delete('Counter');
					$this->redirect(array('controller' => 'products', 'action' => 'cart'));
				}








////////////////////////////////////////////////////////////: GENERATION PDF
				public function viewPdf($id = null)
				{
					if (!$id)
					{
						$this->Session->setFlash('Sorry, there was no property ID submitted.');
	            // $this->redirect(array('action'=>'index'), null, true);
					}

	        Configure::write('debug',0); // Otherwise we cannot use this method while developing

	        $id = intval($id);

	        $property = $this->__view($id); // here the data is pulled from the database and set for the view

	        if (empty($property))
	        {
	        	$this->Session->setFlash('Sorry, there is no property with the submitted ID.');
	        	$this->redirect(array('action'=>'index'), null, true);
	        }

	        $this->layout = 'pdf'; //this will use the pdf.ctp layout
	        $this->render();
	    }

	    public function exportpdf(){
    //pour éviter les outputs du debug de cake
	    	Configure::write('debug',0);
	    	$cart = $this->Session->read('Cart');
    //la solution depuis cakephp 2
	    	$this->response->type('pdf');

	    	$this->layout='pdf';
	    }


	}




		