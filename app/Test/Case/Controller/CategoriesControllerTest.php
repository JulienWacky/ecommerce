<?php
App::uses('CategoriesController', 'Controller');

/**
 * CategoriesController Test Case
 *
 */
class CategoriesControllerTest extends ControllerTestCase {

/**
 * Fixtures
 *
 * @var array
 */
	public $fixtures = array(
		'app.category',
		'app.product',
		'app.brand',
		'app.cart'
	);

/**
 * testIndex method
 *
 * @return void
 */
	public function testIndex() {
	}

/**
 * testView method
 *
 * @return void
 */
	public function testView() {
	}

/**
 * testAdd method
 *
 * @return void
 */
	public function testAdd() {
	}

/**
 * testEdit method
 *
 * @return void
 */
	public function testEdit() {
	}

/**
 * testDelete method
 *
 * @return void
 */
	public function testDelete() {
	}

/**
 * testMembresIndex method
 *
 * @return void
 */
	public function testMembresIndex() {
	}

/**
 * testMembresView method
 *
 * @return void
 */
	public function testMembresView() {
	}

/**
 * testMembresAdd method
 *
 * @return void
 */
	public function testMembresAdd() {
	}

/**
 * testMembresEdit method
 *
 * @return void
 */
	public function testMembresEdit() {
	}

/**
 * testMembresDelete method
 *
 * @return void
 */
	public function testMembresDelete() {
	}

}
