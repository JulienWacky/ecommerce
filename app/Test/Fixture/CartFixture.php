<?php
/**
 * CartFixture
 *
 */
class CartFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'sessionid' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'product_id' => array('type' => 'integer', 'null' => true, 'default' => null),
		'name' => array('type' => 'string', 'null' => true, 'default' => null, 'collate' => 'utf8_unicode_ci', 'charset' => 'utf8'),
		'weight' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '6,2'),
		'price' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '6,2'),
		'quantity' => array('type' => 'integer', 'null' => true, 'default' => null),
		'weight_total' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '6,2'),
		'subtotal' => array('type' => 'float', 'null' => true, 'default' => null, 'length' => '6,2'),
		'created' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => true, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_unicode_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'sessionid' => 'Lorem ipsum dolor sit amet',
			'product_id' => 1,
			'name' => 'Lorem ipsum dolor sit amet',
			'weight' => 1,
			'price' => 1,
			'quantity' => 1,
			'weight_total' => 1,
			'subtotal' => 1,
			'created' => '2014-02-24 09:23:04',
			'modified' => '2014-02-24 09:23:04'
		),
	);

}
