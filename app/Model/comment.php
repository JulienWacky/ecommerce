<?php

class Comment extends AppModel
{
	var $name='Comment';
	var $belongsTo='Post';
	var $validate = array(
		'pseudo' => array(
			'alphaNumeric' => array(
				'rule' => 'alphaNumeric',
				'message' => 'Pseudo non valide'
				),
			'between' => array(
				'rule' => array('between', 3, 15),
				'message' => 'Votre pseudo doit etre composé entre 3 et 15 caractères'
				)
			),
		'mail' => array(
			'rule' => 'email',
			'required' => false,
			'allowEmpty' => true,
			'message' => "Votre email n'est pas valide"
		)
	);

	/*function beforeSave()
	{
		App::import('Sanitize');
		$this->data['Comment']['contenu'] = Sanitize::html($this->data['Comment']['contenu']);
		return true;
	}*/
}

?>