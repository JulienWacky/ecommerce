<?php
App::uses('AppModel', 'Model');
class Order extends AppModel {

//////////////////////////////////////////////////

	public $validate = array(
		'first_name' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'first_name is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'last_name' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'last_name is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'email' => array(
			// 'email' => array(
			// 	'rule' => array('email'),
			// 	'message' => 'Email is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'phone' => array(
			// 'notempty' => array(
			// 	'rule' => array('phone'),
			// 	'message' => 'Phone is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'billing_address' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Billing Address is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'billing_city' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Billing City is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'billing_zip' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Billing Zip is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'billing_country' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Billing country is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'shipping_address' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Shipping Address is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'shipping_city' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Shipping City is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'shipping_zip' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Shipping Zip is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'shipping_country' => array(
			// 'notempty' => array(
			// 	'rule' => array('notempty'),
			// 	'message' => 'Shipping country is invalid',
				//'allowEmpty' => false,
				//'required' => true,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			// ),
		),
		'code_secu' => array(
		),
		'numero_carte' => array(
		),
		'date_expiration' => array(
		),
		// 'creditcard_number' => array(
		// 	'notempty' => array(
		// 		'rule' => array('cc'),
		// 		'message' => 'Credit Card Number is invalid',
		// 		//'allowEmpty' => false,
		// 		//'required' => true,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
		// 'creditcard_code' => array(
		// 	'rule1' => array(
		// 		'rule' => array('notEmpty'),
		// 		'message' => 'Credit Card Code is required',
		// 		//'allowEmpty' => false,
		// 		//'required' => true,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// 	'rule2' => array(
		// 		 'rule' => '/^[0-9]{3,4}$/i',
		// 		 'message' => 'Credit Card Code is invalid',
		// 		//'allowEmpty' => false,
		// 		//'required' => true,
		// 		//'last' => false, // Stop validation after this rule
		// 		//'on' => 'create', // Limit validation to 'create' or 'update' operations
		// 	),
		// ),
	);

//////////////////////////////////////////////////

	// public $hasMany = array(
	// 	'OrderItem' => array(
	// 		'className' => 'OrderItem',
	// 		'foreignKey' => 'order_id',
	// 		'dependent' => true,
	// 		'conditions' => '',
	// 		'fields' => '',
	// 		'order' => '',
	// 		'limit' => '',
	// 		'offset' => '',
	// 		'exclusive' => '',
	// 		'finderQuery' => '',
	// 		'counterQuery' => '',
	// 	)
	// );

//////////////////////////////////////////////////

}
